package data;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by orang_000 on 11/26/2016.
 */
public class GameData {
    public String userName;
    public String password;
    public Map<String,ArrayList<Integer>> status;

    public Map<String, ArrayList<Integer>> getStatus() {
        return status;
    }

    public void setStatus(Map<String, ArrayList<Integer>> status) {
        this.status = status;
    }

    public String getPassword() {
        return password;

    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "GameData{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
