package controller;

/**
 * Created by orang_000 on 11/13/2016.
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/** Main application class for fruit combo fxml demo application */
public class testFile extends Application {
    public int i;
    public static Stage s;

        @FXML //  fx:id="home"
        private Button home; // Value injected by FXMLLoader

        @FXML //  fx:id="login"
        private Button login; // Value injected by FXMLLoader

        @FXML //  fx:id="pause"
        private Button pause; // Value injected by FXMLLoader

        @FXML //  fx:id="pearImage"
        private Button newProfile; // Value injected by FXMLLoader

        @FXML
        private Button confirm;

        @FXML
        private Button levelPlay1;
        @FXML
        private Button levelPlay2;
        @FXML
        private Button levelPlay3;
        @FXML
        private Button levelPlay4;

        @FXML
        private Button modeD, modeP, modeS;
        @FXML
        private Button home2, logout;



        @FXML
        public void handleLogoutRequest() {
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void handleHomeRequest() {
            i = 1;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        public void handleLoginRequest(){
            i = 2;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        public void handleNewProfileRequest(){
            i = 3;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void handleLevelSelection(){
            i = 5;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        public void handleModeSelection(){
            i = 4;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void handleGamePlay(){
            i = 6;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        public void handlePauseRequest(){
            i = 6;
            try {
                start(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    @Override public void start(Stage stage) throws IOException {
        s = stage;

        stage.setTitle("Buzzword");
        AnchorPane layout = FXMLLoader.load(
                new URL(testFile.class.getResource("home_main.fxml").toExternalForm())
        );
        stage.setScene(new Scene(layout));
        stage.show();

        if(i == 1) {
            layout = FXMLLoader.load(
                    new URL(testFile.class.getResource("home_main.fxml").toExternalForm())
            );
            s.setScene(new Scene(layout));
            s.show();
        }
        if(i == 2) {
            layout = FXMLLoader.load(
                    new URL(testFile.class.getResource("login.fxml").toExternalForm())
            );
            s.setScene(new Scene(layout));
            s.show();
        }
        if(i == 3) {
            layout = FXMLLoader.load(
                    new URL(testFile.class.getResource("createProfile.fxml").toExternalForm())
            );
            s.setScene(new Scene(layout));
            s.show();
        }
        if(i == 4) {
            layout = FXMLLoader.load(
                    new URL(testFile.class.getResource("modeSelection.fxml").toExternalForm())
            );
            s.setScene(new Scene(layout));
            s.show();
        }
        if(i == 5) {
            layout = FXMLLoader.load(
                    new URL(testFile.class.getResource("levelSelection.fxml").toExternalForm())
            );
            s.setScene(new Scene(layout));
            s.show();
        }
        if(i == 6) {
            layout = FXMLLoader.load(
                    new URL(testFile.class.getResource("gamePlay.fxml").toExternalForm())
            );
            s.setScene(new Scene(layout));
            s.show();
        }
    }
}