package controller;

import buzzword.Scene;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by orang_000 on 12/10/2016.
 */
public class helpController implements Initializable {
    @FXML
    private Button home, close, home2;


    Scene openScene = new Scene();
    public void handleHome(){
        openScene.openScene("modeSelection",true);
    }
    public void handleClose(){
        System.exit(0);
    }
    public void handleLogoutRequest(){
        openScene.openScene("home_main",true);
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
