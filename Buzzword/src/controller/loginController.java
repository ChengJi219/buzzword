package controller;

import buzzword.Scene;
import data.GameData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import com.alibaba.fastjson.JSON;
import javafx.scene.text.Text;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class loginController implements Initializable{

    public static String usernameDisplay;
    @FXML
    private Button confirm;
    @FXML
    private TextArea userName;
    @FXML
    private PasswordField password;
    @FXML
    private Text warning;
    @FXML
    private Button close, home;

    public static GameData user;
    public static String passwordToview;

    Scene open = new Scene();
    public void handleHome(){
        open.openScene("home_main", true);
    }

    public void handleModeSelection(){
        try {
            warning.setVisible(false);
            Scene openScene = new Scene();
            List<GameData> peopleData = null;
            File file = new File("UsersData.txt");
            String data = FileUtils.readFileToString(file);
            peopleData = JSON.parseArray(data,GameData.class);

            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(password.getText().getBytes());
            byte[] messageDigestMD5 = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }

            for (int i = 0; i < peopleData.size(); i++) {
                if(peopleData.get(i).getUserName().equals(userName.getText())) {
                    if (peopleData.get(i).getPassword().equals(stringBuffer.toString())) {
                        user = peopleData.get(i);
                        usernameDisplay = userName.getText();
                        if(!userName.getText().equals("") && !password.getText().equals("")) {
                            passwordToview = password.getText();
                            openScene.openScene("modeSelection", true);
                        }
                    } else {
                        warning.setVisible(true);
                    }
                }
            }
            warning.setVisible(true);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


    }
    public void handleClose(){
        System.exit(0);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        home.setText("HOME");
    }
}
