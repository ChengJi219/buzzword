package controller;

import buzzword.Scene;
import com.alibaba.fastjson.JSON;
import data.GameData;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.TimerTask;
import java.util.Timer;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class gamePlayController implements Initializable{
    @FXML
    private Button logout;
    @FXML
    private Button close;
    @FXML
    private Label target;
    @FXML
    private Label levelLabel;
    @FXML
    private Button home2;
    @FXML
    private Button pause;
    @FXML
    private Circle cir1,cir2,cir3,cir4,cir5,cir6,cir7,cir8,cir9,cir10,cir11,cir12,cir13,cir14,cir15,cir16;
    @FXML
    private Label lab1,lab2,lab3,lab4,lab5,lab6,lab7,lab8,lab9,lab10,lab11,lab12,lab13,lab14,lab15,lab16;

    @FXML
    private GridPane circlePane;

    //creating timer
    @FXML
    private Label timerLabel;
    //key typed label
    @FXML
    private TextField textType;
    @FXML
    private ListView tableInfo;
    @FXML
    private Label totalScore;

    @FXML
    private Button replay;

    @FXML
    private Button next;
    @FXML
    private Label mode;

    private URL location;


    private static javafx.scene.Scene keepScene;
    private static final Integer STARTTIME = 15;
    public boolean pauseYN = false;
    private IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
    //public String targetScore;
    char[][] board = new char[4][4];
    char[] text = new char[16];
    ArrayList<String> trials = new ArrayList<>();
    ArrayList<String> totalWords = new ArrayList<>();
    private ArrayList<Character> letters = new ArrayList<>(Arrays.asList('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'));
    private Timeline timeline;
    private Integer score = 0, totalNumber = 0;
    private ArrayList<Label> labels;
    private ArrayList<Circle> circles;
    //private String trueTarget = target.getText().substring(8);
    private Map<String, ArrayList<Integer>> currentStatus = new Map<String, ArrayList<Integer>>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsKey(Object key) {
            return false;
        }

        @Override
        public boolean containsValue(Object value) {
            return false;
        }

        @Override
        public ArrayList<Integer> get(Object key) {
            return null;
        }

        @Override
        public ArrayList<Integer> put(String key, ArrayList<Integer> value) {
            return null;
        }

        @Override
        public ArrayList<Integer> remove(Object key) {
            return null;
        }

        @Override
        public void putAll(Map<? extends String, ? extends ArrayList<Integer>> m) {

        }

        @Override
        public void clear() {

        }

        @Override
        public Set<String> keySet() {
            return null;
        }

        @Override
        public Collection<ArrayList<Integer>> values() {
            return null;
        }

        @Override
        public Set<Entry<String, ArrayList<Integer>>> entrySet() {
            return null;
        }
    };
    Scene openScene = new Scene();
    private StringProperty keys = new SimpleStringProperty();
    private Integer colorFlag;
    private boolean firstKey;
    private String toSearch;
    private boolean hitEnter = false;
    private ArrayList<Object> mouseTouched = new ArrayList<>();
    private String mouseResult = "";
    private String mouseConcat = "";
    private Integer mouseFlag;
    FileReader fr, fr1, fr2;
    BufferedReader br, br1, br2;
    private static String paneConcat = "";
    private Integer circleNumber;
    Map<String,ArrayList<String>> dictionaryMaps = new HashMap<>();
    ArrayList<String> strDictionary = new ArrayList<>();



    public void handleReplay(){
        timeline.stop();
        openScene.openScene("gamePlay",true);

//        initialize(this.location,null);
    }
    public void handleNext(){
        while (levelSelectionController.levelFlag < 8){
            levelSelectionController.levelFlag = levelSelectionController.levelFlag + 1;
            break;
        }

        initialize(this.location,null);
        reInitialize();

    }

    public void handleClose(){
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setContentText("Are you sure you want to quit?");
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    public void handleLogoutRequest(){
        timeline.stop();
        openScene.openScene("home_main", true);
    }
    public void handleModeSelection(){
        timeline.stop();
        openScene.openScene("modeSelection", true);
    }
    public void handlePauseRequest(){
        Random r = new Random();
        ArrayList<Label> labels = new ArrayList<Label>(Arrays.asList(lab1,lab2,lab3,lab4,lab5,lab6,lab7,lab8,lab9,lab10,lab11,lab12,lab13,lab14,lab15,lab16));
        if(pauseYN == false) {
            for (int i = 0; i < labels.size(); i++) {
                labels.get(i).setVisible(false);
            }
            pauseYN = true;
            timeline.stop();
            pause.setText("resume");

        }else {
            for (int i = 0; i < labels.size(); i++) {
                labels.get(i).setVisible(true);
            }
            pauseYN = false;
            timeline.play();
            pause.setText("pause");
        }
    }
    public boolean findWord (String word){
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                if(findWord(word, row, column)){
                    if (!totalWords.contains(word)) {
                        totalWords.add(word);
                    }
                    return true;
                }
            }
        }
        return false;
    }
    public void findWordPlaying (String word){
        int counterFind = 0;
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                while(counterFind <= 16){
                    findWord(word,row,column);
                    counterFind = counterFind + 1;
                    break;
                }
            }
        }
    }

    private boolean findWord(String word, int row, int column) {

        if (word.equals("")){
            return true;
        }else if(row < 0 || row >= board.length ||column < 0 ||
                 column >= board.length||
                board[row][column] != word.charAt(0)){

            return false;
        }else{
            char safe = this.board[row][column];
            board[row][column] = '*';
            String leftOver = word.substring(1, word.length());
            boolean result = (findWord(leftOver,row - 1, column - 1)||
                              findWord(leftOver,row - 1, column)||
                              findWord(leftOver,row - 1, column + 1)||
                              findWord(leftOver,row, column - 1)||
                              findWord(leftOver,row, column + 1)||
                              findWord(leftOver,row + 1, column - 1)||
                              findWord(leftOver,row + 1, column + 1));
            board[row][column] = safe;

            if (result == true && colorFlag == 1){
                colorPostion(row, column);
                //System.out.println(row + " " + column);
            }
            return result;
        }
    }
    public void generate(){
        ArrayList<Circle> circles = new ArrayList<>(Arrays.asList(cir1,cir2,cir3,cir4,cir5,cir6,cir7,cir8,cir9,cir10,cir11,cir12,cir13,cir14,cir15,cir16));
        ArrayList<Label> labels = new ArrayList<Label>(Arrays.asList(lab1,lab2,lab3,lab4,lab5,lab6,lab7,lab8,lab9,lab10,lab11,lab12,lab13,lab14,lab15,lab16));
        Random r = new Random();
        for (int i = 0; i < circles.size(); i++) {
            labels.get(i).setText(letters.get(r.nextInt(25-0)).toString());
            text[i] = labels.get(i).getText().charAt(0);
        }



        int textCounter = 0;
//        for (int i = 0; i < text.length; i++) {
//            System.out.print(text[i]);
//        }
//        System.out.println();
        //finish array filling
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                board[row][column] = text[textCounter];
                textCounter++;
            }
        }

    }
    public void colorPostion(int row, int column){
        if (row == 0 && column == 0){
            circles.get(0).setFill(Color.web("ffccfd"));
        }else if (row == 0 && column == 1){
            circles.get(1).setFill(Color.web("ffccfd"));
        }else if (row == 0 && column == 2){
            circles.get(2).setFill(Color.web("ffccfd"));
        }else if (row == 0 && column == 3){
            circles.get(3).setFill(Color.web("ffccfd"));
        }else if (row == 1 && column == 0){
            circles.get(4).setFill(Color.web("ffccfd"));
        }else if (row == 1 && column == 1){
            circles.get(5).setFill(Color.web("ffccfd"));
        }else if (row == 1 && column == 2){
            circles.get(6).setFill(Color.web("ffccfd"));
        }else if (row == 1 && column == 3){
            circles.get(7).setFill(Color.web("ffccfd"));
        }else if (row == 2 && column == 0){
            circles.get(8).setFill(Color.web("ffccfd"));
        }else if (row == 2 && column == 1){
            circles.get(9).setFill(Color.web("ffccfd"));
        }else if (row == 2 && column == 2){
            circles.get(10).setFill(Color.web("ffccfd"));
        }else if (row == 2 && column == 3){
            circles.get(11).setFill(Color.web("ffccfd"));
        }else if (row == 3 && column == 0){
            circles.get(12).setFill(Color.web("ffccfd"));
        }else if (row == 3 && column == 1){
            circles.get(13).setFill(Color.web("ffccfd"));
        }else if (row == 3 && column == 2){
            circles.get(14).setFill(Color.web("ffccfd"));
        }else if (row == 3 && column == 3){
            circles.get(15).setFill(Color.web("ffccfd"));
        }
    }

    public void handleTypeLetter(KeyEvent ae){

        if (firstKey != true){
            for (int i = 0; i < circles.size(); i++) {
                circles.get(i).setFill(Color.web("ff99fb"));
            }
        }
        toSearch = toSearch.concat(ae.getCharacter());
        //System.out.println(toSearch);
        toSearch = toSearch.replaceAll("\r", "");
        toSearch = toSearch.replaceAll("\b", "");
        findWordPlaying(toSearch);
        firstKey = false;

//        for (int i = 0; i < this.labels.size(); i++) {
//            if (ae.getCharacter().equals(this.labels.get(i).getText())){
//                circles.get(i).setFill(Color.web("#ffccfd"));
//            }
//        }
    }

    public void onEnter(ActionEvent ae){
        for (int i = 0; i < totalWords.size(); i++) {
            if (textType.getText().equals(totalWords.get(i))){
                if (!tableInfo.getItems().toString().contains(textType.getText())) {
                    tableInfo.getItems().add(totalWords.get(i) + " " + (textType.getLength() - 2) * 10);
                    score = score + ((textType.getLength() - 2) * 10);
                    totalScore.setText("Total: " + score);
                }
            }
        }
        textType.setText("");
        toSearch = "";
        firstKey = true;
        hitEnter = true;
        //clear color
        for (int i = 0; i < circles.size(); i++) {
            circles.get(i).setFill(Color.web("ff99fb"));
        }

    }
//
//    public void play() {
//        AnimationTimer timer = new AnimationTimer() {
//
//            @Override
//
//            public void handle(long now) {
//                System.out.println(keepScene);
//                keepScene.setOnKeyTyped((KeyEvent event) -> {
//                    System.out.print("hiiii##############################################33");
//
//
//                });
//            }
//
//        };
//        timer.start();
//
//    }
    //mouse events

    public void onDragged (MouseEvent me){
//        //gap: 46, circle: 40
//
////        if((me.getX() + 20) % 86 =)
//
//
//
//
//        if (me.getX() > 0 && me.getX() < 39 && me.getY() > 0 && me.getY() < 32){
//            mouseTouched.add(labels.get(0));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(0).getText());
//            textType.setText(paneConcat);
//
//        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 0 && me.getY() < 32){
//            mouseTouched.add(labels.get(1));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(1).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 0 && me.getY() < 32){
//            mouseTouched.add(labels.get(2));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(2).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 0 && me.getY() < 32){
//            mouseTouched.add(labels.get(3));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(3).getText());
//            textType.setText(paneConcat);
//        }
//        //row 2
//        else if (me.getX() > 0 && me.getX() < 39 && me.getY() > 79.6 && me.getY() < 111.6){
//            mouseTouched.add(labels.get(4));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(4).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 79.6 && me.getY() < 111.6){
//            mouseTouched.add(labels.get(5));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(5).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 79.6 && me.getY() < 111.6){
//            mouseTouched.add(labels.get(6));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(6).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 79.6 && me.getY() < 111.6){
//            mouseTouched.add(labels.get(7));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(7).getText());
//            textType.setText(paneConcat);
//        }else if (me.getX() > 0 && me.getX() < 39 && me.getY() > 159.2 && me.getY() < 191.2){
//            mouseTouched.add(labels.get(8));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(8).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 159.2 && me.getY() < 191.2){
//            mouseTouched.add(labels.get(9));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(9).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 159.2 && me.getY() < 191.2){
//            mouseTouched.add(labels.get(10));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(10).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 159.2 && me.getY() < 191.2){
//            mouseTouched.add(labels.get(11));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(11).getText());
//            textType.setText(paneConcat);
//        }else if (me.getX() > 0 && me.getX() < 39 && me.getY() > 238.8 && me.getY() < 271){
//            mouseTouched.add(labels.get(12));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(12).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 238.8 && me.getY() < 271){
//            mouseTouched.add(labels.get(13));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(13).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 238.8 && me.getY() < 271){
//            mouseTouched.add(labels.get(14));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(14).getText());
//            textType.setText(paneConcat);
//        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 238.8 && me.getY() < 271){
//            mouseTouched.add(labels.get(15));//add label
//            colorMouse();
//            paneConcat = paneConcat.concat(labels.get(15).getText());
//
//            textType.setText(paneConcat);
//        }
//
    }

    public void onOver(MouseEvent me) {



        if (me.getX() > 0 && me.getX() < 39 && me.getY() > 0 && me.getY() < 32){
            circleNumber = 0;
            if (!mouseTouched.contains(labels.get(0)) && compareDistance(labels.get(0).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(0));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(0).getText());

            }

        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 0 && me.getY() < 32){
            circleNumber = 1;
            if (!mouseTouched.contains(labels.get(1)) && compareDistance(labels.get(1).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray()) ){

                mouseTouched.add(labels.get(1));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(1).getText());

            }

        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 0 && me.getY() < 32){
            circleNumber = 2;
            if (!mouseTouched.contains(labels.get(2)) && compareDistance(labels.get(2).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){


                mouseTouched.add(labels.get(2));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(2).getText());
            }

        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 0 && me.getY() < 32){
            circleNumber = 3;
            if (!mouseTouched.contains(labels.get(3))&&compareDistance(labels.get(3).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(3));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(3).getText());

            }
        }
        //row 2
        else if (me.getX() > 0 && me.getX() < 39 && me.getY() > 79.6 && me.getY() < 111.6){
            circleNumber = 4;
            if (!mouseTouched.contains(labels.get(4))&&compareDistance(labels.get(4).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(4));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(4).getText());

            }
        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 79.6 && me.getY() < 111.6){
            circleNumber = 5;
            if (!mouseTouched.contains(labels.get(5))&&compareDistance(labels.get(5).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(5));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(5).getText());

            }
        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 79.6 && me.getY() < 111.6){
            circleNumber = 6;
            if (!mouseTouched.contains(labels.get(6))&&compareDistance(labels.get(6).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(6));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(6).getText());

            }
        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 79.6 && me.getY() < 111.6){
            circleNumber = 7;
            if (!mouseTouched.contains(labels.get(7))&&compareDistance(labels.get(7).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(7));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(7).getText());

            }
        }else if (me.getX() > 0 && me.getX() < 39 && me.getY() > 159.2 && me.getY() < 191.2){
            circleNumber = 8;
            if (!mouseTouched.contains(labels.get(8))&&compareDistance(labels.get(8).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(8));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(8).getText());

            }
        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 159.2 && me.getY() < 191.2){
            circleNumber = 9;
            if (!mouseTouched.contains(labels.get(9))&&compareDistance(labels.get(9).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(9));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(9).getText());


            }
        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 159.2 && me.getY() < 191.2){
            circleNumber = 10;
            if(!mouseTouched.contains(labels.get(10)) && compareDistance(labels.get(10).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray()) ){

                mouseTouched.add(labels.get(10));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(10).getText());

            }
        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 159.2 && me.getY() < 191.2){
            circleNumber = 11;
            if (!mouseTouched.contains(labels.get(11))&&compareDistance(labels.get(11).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(11));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(11).getText());

            }
        }else if (me.getX() > 0 && me.getX() < 39 && me.getY() > 238.8 && me.getY() < 271){
            circleNumber = 12;
            if (!mouseTouched.contains(labels.get(12))&&compareDistance(labels.get(12).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(12));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(12).getText());

            }
        } else if (me.getX() > 86.6 && me.getX() < 125.6 && me.getY() > 238.8 && me.getY() < 271){
            circleNumber = 13;
            if (!mouseTouched.contains(labels.get(13))&&compareDistance(labels.get(13).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(13));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(13).getText());

            }
        } else if (me.getX() > 173.2 && me.getX() < 212.2 && me.getY() > 238.8 && me.getY() < 271){
            circleNumber = 14;
            if (!mouseTouched.contains(labels.get(14))&&compareDistance(labels.get(14).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(14));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(14).getText());

            }
        } else if (me.getX() > 259.8 && me.getX() < 299 && me.getY() > 238.8 && me.getY() < 271){
            circleNumber = 15;
            if (!mouseTouched.contains(labels.get(15))&&compareDistance(labels.get(15).getText().toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray())){
                mouseTouched.add(labels.get(15));//add label
                colorMouse();
                paneConcat = paneConcat.concat(labels.get(15).getText());

            }
        }

    }

    public void onReleased (MouseEvent me){
        mouseConcat = "";
        for (int i = 0; i < mouseTouched.size(); i++) {
            mouseConcat = mouseConcat.concat(((Label)mouseTouched.get(i)).getText());
        }
        textType.setText(mouseConcat);
        if(totalWords.contains(mouseConcat)){
            if (!tableInfo.getItems().toString().contains(mouseConcat)) {
                tableInfo.getItems().add(mouseConcat + " " + (mouseConcat.length() - 2) * 10);
                score = score + ((mouseConcat.length() - 2) * 10);
                totalScore.setText("Total: " + score);
            }
        }
        clearCell();
        mouseConcat = "";
        mouseTouched = new ArrayList<>();
        textType.clear();
    }


    public void detectHandle(MouseEvent me){
//        //System.out.println(me.getSource().toString());
        mouseFlag = 1;
        ((Label) me.getSource()).startFullDrag();

        mouseTouched.add(((Label) me.getSource()));

        mouseConcat = mouseConcat.concat(((Label) me.getSource()).getText());
        textType.setText(mouseConcat);
        colorMouse();
//        //System.out.println("detect");

    }
    public void doneHandle(){
        //System.out.println("done");
    }
    public void enterHandle(){
        //System.out.println("enter");
    }
    public void dropHandle(){
        //System.out.println("drop");
    }
    public void mouseEnterHandle(MouseEvent me){

//        if (!mouseTouched.contains(me.getSource())) {
//            mouseTouched.add(me.getSource());
//        }
//
////        ((Circle) me.getSource()).setFill(Color.web("#ffccfd"));
//        colorMouse();
//        System.out.println("mouse enter" + ((Label) me.getSource()).getText());
//
//        //System.out.println("mouse over");
//        if (!mouseTouched.contains(me.getSource()) && (compareDistance((((Label) me.getSource()).getText()).toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray()))) {
//            mouseTouched.add(me.getSource());
//            mouseConcat = mouseConcat.concat(((Label) me.getSource()).getText());
//            textType.setText(mouseConcat);
//        }
//
////        ((Circle) me.getSource()).setFill(Color.web("#ffccfd"));
//        colorMouse();
    }
    public void mouseExitHandle(MouseEvent me){
//        System.out.println("mouse exit");
//        for (int i = 0; i < mouseTouched.size(); i++) {
//            System.out.println(mouseTouched.get(i));
//        }



    }
    public boolean compareDistance(char[] now, char[] previous){






        int row1 = 0, row2 = 0, col1 = 0, col2 = 0;
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board.length; column++) {
                if ((4*row + column) == circleNumber){//now
                    row1 = row;
                    col1 = column;

                }
//                if ((( board[row][column]) == previous[0])){
                if (Integer.parseInt(((Label) mouseTouched.get(mouseTouched.size() - 1)).getId().substring(3,((Label) mouseTouched.get(mouseTouched.size() - 1)).getId().length())) - 1 == (4*row + column)){

                    row2 = row;
                    col2 = column;

                }
            }
        }
        if (Math.abs(row1 - row2) <= 1 && Math.abs(col1 - col2) <= 1){
            return true;
        }
        return false;

    }
    public void mouseOverHandle(MouseEvent me){
//        if (!mouseTouched.contains(me.getSource()) && (compareDistance((((Label) me.getSource()).getText()).toCharArray(), ((Label) mouseTouched.get(mouseTouched.size() - 1)).getText().toCharArray()))) {
//            mouseTouched.add(me.getSource());
//            mouseConcat = mouseConcat.concat(((Label) me.getSource()).getText());
//            textType.setText(mouseConcat);
//        }
//
////        ((Circle) me.getSource()).setFill(Color.web("#ffccfd"));
//        colorMouse();
//        //System.out.println("mouse over");

    }
    public void mouseReleaseHandle(){
//        for (int i = 0; i < mouseTouched.size(); i++) {
//            mouseResult = mouseResult.concat(((Label) mouseTouched.get(i)).getText());
//        }
//        if(totalWords.contains(mouseResult)){
//            if (!tableInfo.getItems().toString().contains(mouseResult)) {
//                tableInfo.getItems().add(mouseResult + " " + (mouseResult.length() - 2) * 10);
//                score = score + ((mouseResult.length() - 2) * 10);
//                totalScore.setText("Total: " + score);
//            }
//        }
//        //System.out.println("mouse release");
//        clearCell();
//        textType.clear();
//        mouseConcat = "";
//        mouseResult = "";
//        mouseTouched = new ArrayList<>();
//        mouseFlag = 0;
    }
    public void colorMouse(){
//        for (int i = 0; i < labels.size(); i++) {
//            circles.get(labels.indexOf((Label)mouseTouched.get(i))).setFill(Color.web("#ffccfd"));
//        }
        for (int i = 0; i < mouseTouched.size() ; i++) {
            circles.get(labels.indexOf((Label)mouseTouched.get(i))).setFill(Color.web("#ffccfd"));
        }


//        for (int i = 0; i < mouseTouched.size(); i++) {
//            circles.indexOf((Label)mouseTouched.get(i)));
//        }
    }
    public void clearCell(){
        for (int i = 0; i < circles.size(); i++) {
            circles.get(i).setFill(Color.web("#ff99fb"));
        }
    }
    public void reInitialize(){
        tableInfo.getItems().clear();
        score = 0;
        totalScore.setText("Total:");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //map dictionary
        dictionaryMaps.put("a",strDictionary);
        dictionaryMaps.put("b",strDictionary);
        dictionaryMaps.put("c",strDictionary);
        dictionaryMaps.put("d",strDictionary);
        dictionaryMaps.put("e",strDictionary);
        dictionaryMaps.put("f",strDictionary);
        dictionaryMaps.put("g",strDictionary);
        dictionaryMaps.put("h",strDictionary);
        dictionaryMaps.put("i",strDictionary);
        dictionaryMaps.put("j",strDictionary);
        dictionaryMaps.put("k",strDictionary);
        dictionaryMaps.put("l",strDictionary);
        dictionaryMaps.put("m",strDictionary);
        dictionaryMaps.put("n",strDictionary);
        dictionaryMaps.put("o",strDictionary);
        dictionaryMaps.put("p",strDictionary);
        dictionaryMaps.put("q",strDictionary);
        dictionaryMaps.put("r",strDictionary);
        dictionaryMaps.put("s",strDictionary);
        dictionaryMaps.put("t",strDictionary);
        dictionaryMaps.put("u",strDictionary);
        dictionaryMaps.put("v",strDictionary);
        dictionaryMaps.put("w",strDictionary);
        dictionaryMaps.put("x",strDictionary);
        dictionaryMaps.put("y",strDictionary);
        dictionaryMaps.put("z",strDictionary);


        if (modeSelectionController.flag == "d"){
            mode.setText("Dictionary");
        } else if (modeSelectionController.flag == "n"){
            mode.setText("Names");
        } else if (modeSelectionController.flag == "s"){
            mode.setText("Science");
        }

        this.location = location;
        firstKey = true;
        toSearch = "";
        //color flag is 0
        colorFlag = 0;
        pause.setDisable(false);
        next.setDisable(true);
        tableInfo.getItems().clear();
        textType.clear();
        textType.setVisible(true);
        textType.setDisable(false);
        //set target



        //initialize display
//        System.out.println("#######################################3");
//        System.out.println(Scene.temp);
//        keepScene = Scene.temp;
//        System.out.println(keepScene);
        logout.setText(loginController.usernameDisplay);
        levelLabel.setText("Level: " + levelSelectionController.levelFlag);

//        String trueTarget = ;

        //set target
        //set target
        if (levelSelectionController.levelFlag == 1){

            target.setText("Target: 30");

        } else if (levelSelectionController.levelFlag == 2){
            target.setText("Target: 50");
        }else if (levelSelectionController.levelFlag == 3){
            target.setText("Target: 70");
        }else if (levelSelectionController.levelFlag == 4){
            target.setText("Target: 90");
        }else if (levelSelectionController.levelFlag == 5){
            target.setText("Target: 110");
        }else if (levelSelectionController.levelFlag == 6){
            target.setText("Target: 130");
        }else if (levelSelectionController.levelFlag == 7){
            target.setText("Target: 150");
        }else if (levelSelectionController.levelFlag == 8){
            target.setText("Target: 170");
        }


//        System.out.println(trueTarget);

        //reading words file
        ArrayList<String> dictionaryD = new ArrayList<>();
        ArrayList<String> dictionaryR = new ArrayList<>();
        ArrayList<String> dictionaryS = new ArrayList<>();
        ArrayList<String> dictionary = new ArrayList<>();
        try {
            fr = new FileReader("C:\\Users\\orang_000\\Desktop\\TheBuzzwordGame\\words.txt");
            fr1 = new FileReader("C:\\Users\\orang_000\\Desktop\\TheBuzzwordGame\\names.txt");
            fr2 = new FileReader("C:\\Users\\orang_000\\Desktop\\TheBuzzwordGame\\science.txt");


            if (modeSelectionController.flag.equals("d") && dictionaryD.size() == 0) {
                br = new BufferedReader(fr);
                String line;

                while ((line = br.readLine()) != null) {
                    if (line.length() >= 3){
                        dictionaryD.add(line.split(":")[0]);
                        dictionaryMaps.put(line.split(":")[0].substring(0,1),strDictionary);
                    }
                }
                dictionary = dictionaryD;

            } else if (modeSelectionController.flag.equals("n") && dictionaryR.size() == 0){
                br1 = new BufferedReader(fr1);
                String line;

                while ((line = br1.readLine()) != null) {
                    if (line.length() >= 3){
                        dictionaryR.add(line.split(":")[0]);
                        dictionaryMaps.put(line.split(":")[0].substring(0,1),strDictionary);
                    }
                }
                dictionary = dictionaryR;

            } else if(modeSelectionController.flag.equals("s") && dictionaryS.size() == 0){
                br2 = new BufferedReader(fr2);
                String line;

                while ((line = br2.readLine()) != null) {
                    if (line.length() >= 3){
                        dictionaryS.add(line.split(":")[0]);
                        dictionaryMaps.put(line.split(":")[0].substring(0,1),strDictionary);
                    }
                }
                dictionary = dictionaryS;

            }


//        String line;
//
//            while ((line = br.readLine()) != null) {
//                if (line.length() >= 3){
//                    dictionary.add(line.split(":")[0]);
//                }
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //create new instances
        ArrayList<Circle> circles = new ArrayList<>(Arrays.asList(cir1,cir2,cir3,cir4,cir5,cir6,cir7,cir8,cir9,cir10,cir11,cir12,cir13,cir14,cir15,cir16));
        ArrayList<Label> labels = new ArrayList<Label>(Arrays.asList(lab1,lab2,lab3,lab4,lab5,lab6,lab7,lab8,lab9,lab10,lab11,lab12,lab13,lab14,lab15,lab16));

        for (int i = 0; i < circles.size(); i++) {
            circles.get(i).setDisable(true);
        }


        this.labels = labels;
        this.circles = circles;

        Random r = new Random();
        for (int i = 0; i < circles.size(); i++) {
            labels.get(i).setText(letters.get(r.nextInt(25-0)).toString());
            text[i] = labels.get(i).getText().charAt(0);
        }

        int textCounter = 0;

        //finish array filling
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                board[row][column] = text[textCounter];
                textCounter++;
            }
        }



        for (int i = 0; i < dictionary.size(); i++) {
            findWord(dictionary.get(i));
        }
        //print total test
        for (int i = 0; i < totalWords.size(); i++) {
//            System.out.println(totalWords.get(i));
        }

//        System.out.println(totalWords.size()*10 + " " + Integer.parseInt(target.getText().substring(8,target.getText().length() - 1)));
        //get total score
        for (int i = 0; i < totalWords.size(); i++) {
            totalNumber = totalNumber + ((totalWords.get(i).length() - 2)*10);
        }

        while (totalNumber < Integer.parseInt(target.getText().substring(8,target.getText().length() - 1))){
            generate();
        }



        colorFlag = 1;
        //timer display and handle actions
        timerLabel.textProperty().bind(timeSeconds.asString().concat(" < TIME"));
        if (timeline != null) {
            timeline.stop();
        }
        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME+1),
                        new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();

        timeline.setOnFinished(new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < labels.size(); i++) {
                            circles.get(i).setOnDragDetected(null);
                            labels.get(i).setOnDragDetected(null);
                        }
                        clearCell();
                        textType.clear();
                        textType.setDisable(true); //last word does not count
                        tableInfo.getItems().clear();
                        for (int i = 0; i < totalWords.size(); i++) {
                            tableInfo.getItems().add(totalWords.get(i) + " " + (totalWords.get(i).length() - 2) * 10);
                        }
                        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
                        //need to determine if target score is reached then call actions base on that
                        if (score >= Integer.parseInt(target.getText().substring(8))) {
                            //temp has the current status that is to be changed
                            ArrayList<Integer> temp = loginController.user.getStatus().get(levelSelectionController.modeFlag);
                            if (levelSelectionController.levelFlag == temp.get(0)){
                                temp.set(0, temp.get(0) + 1);//set the level completion to the next level
                            }
                            if (score > temp.get(levelSelectionController.levelFlag)){
                                dialog.setContentText("You set a new record!");
                                dialog.showAndWait();
                                temp.set(levelSelectionController.levelFlag, score);
                            }

                            //save the current status of the game, level + 1 and check if it is the highest score so far
                            currentStatus = loginController.user.status;
                            currentStatus.put(levelSelectionController.modeFlag,temp);
                            loginController.user.setStatus(currentStatus);


                            //save current status
                            List<GameData> peopleList = null;
                            File file = new File("UsersData.txt");
                            String data = null;
                            try {
                                data = FileUtils.readFileToString(file);
                                if (data == null || data.length() == 0){
                                    peopleList = new ArrayList<>();
                                }else{
                                    peopleList = JSON.parseArray(data, GameData.class);
                                }
                                if (peopleList == null || peopleList.size() == 0){
                                    peopleList = new ArrayList<>();
                                }
                                for (int i = 0; i < peopleList.size(); i++) {
                                    if(peopleList.get(i).getUserName().equals(loginController.user.getUserName())){
                                        peopleList.set(i, loginController.user);
                                    }
                                }
                                String fromJson = JSON.toJSONString(peopleList);
                                FileUtils.writeStringToFile(file,fromJson);
                                if (levelSelectionController.levelFlag != 8){
                                    next.setDisable(false);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        }else{
                            dialog.setContentText("You lost this round :(");
                            Optional<ButtonType> result = dialog.showAndWait();
                            pause.setDisable(true);
//                            if (result.get() == ButtonType.OK) {
//                                openScene.openScene("levelSelection",true);
//                            }

                        }
                    }
                });
            }
        });

    }

}
