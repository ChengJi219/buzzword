package controller;

import buzzword.Scene;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class homeController implements Initializable{
    @FXML
    private Button newProfile;
    @FXML
    private Button login;
    @FXML
    private Button close;

    Scene openScene = new Scene();
    public void handleNewProfileRequest(){
        openScene.openScene("createProfile", true);

    }
    public void handleLoginRequest(){
        openScene.openScene("login",true);
    }

    public void handleClose(){
        System.exit(0);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
