package controller;

import buzzword.Scene;
import data.GameData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import com.alibaba.fastjson.JSON;
import javafx.scene.text.Text;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class createProfileController implements Initializable{
    @FXML
    private Button confirm; // confirm the new user profile
    @FXML
    private TextArea userName;
    @FXML
    private PasswordField password;
    @FXML
    private Text warning;
    @FXML
    private Button close, home;

    private ArrayList<String> userNames = new ArrayList<String>();
    private ArrayList<String> passwords= new ArrayList<String>();
    Scene open = new Scene();
    public void handleHome(){
        open.openScene("home_main",true);
    }

    public void handleClose(){
        System.exit(0);
    }

    public void handleModeSelection(){
        try {
            ArrayList<Integer> scores = new ArrayList<Integer>(Arrays.asList(1,0,0,0,0,0,0,0,0));
            GameData newPerson = new GameData();
            newPerson.setUserName(userName.getText());
            //password encrypted
//            MessageDigest md5Password = MessageDigest.getInstance("MD5");
//            md5Password.update(password.getText().getBytes(),0,password.getText().length());
//            newPerson.setPassword(new BigInteger(1,md5Password.digest()).toString(16));

            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(password.getText().getBytes());
            byte[] messageDigestMD5 = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }

            //set the encrypted password
            newPerson.setPassword(stringBuffer.toString());
            Map<String,ArrayList<Integer>> maps = new HashMap<>();
            maps.put("Dictionary", scores);
            maps.put("Names", scores);
            maps.put("Science", scores);
            newPerson.setStatus(maps);
            List<GameData> peopleList = null;
            File file = new File("UsersData.txt");
            String data = FileUtils.readFileToString(file);
            if (data == null || data.length() == 0){
                peopleList = new ArrayList<>();
            }else{
                peopleList = JSON.parseArray(data, GameData.class);
            }
            if (peopleList == null || peopleList.size() == 0){
                peopleList = new ArrayList<>();
            }
            boolean exist = false;
            for (int i = 0; i < peopleList.size(); i++) {
                if(peopleList.get(i).getUserName().equals(userName.getText())){
                    warning.setVisible(true);
                    exist = true;
                    break;
                }
            }
            if(exist == false){
                peopleList.add(newPerson);
                loginController.user = newPerson;
                String fromJson = JSON.toJSONString(peopleList);
                FileUtils.writeStringToFile(file,fromJson);
                Scene openScene = new Scene();
                openScene.openScene("modeSelection",true);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        home.setText("HOME");

    }
}
