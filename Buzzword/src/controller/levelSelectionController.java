package controller;

import buzzword.Scene;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class levelSelectionController implements Initializable{
    @FXML
    private Button levelplay1,levelplay2,levelplay3,levelplay4,levelplay5,levelplay6,levelplay7,levelplay8;
    @FXML
    private Button logout;
    @FXML
    private Button home2;
    @FXML
    private Label warning;
    @FXML
    private Button close;


    public static int levelFlag;
    public static String modeFlag;


    Scene openScene = new Scene();
    public void handleClose(){
        System.exit(0);
    }

    public void handleLogoutRequest(){
        openScene.openScene("home_main",true);
    }
    public void handleModeSelection(){
        openScene.openScene("modeSelection",true);
    }

    public void handleGamePlay(javafx.event.ActionEvent e){
        String source = ((Button)e.getSource()).getText();
        levelFlag = Integer.parseInt(source);

        if (modeSelectionController.flag == "d"){
            modeFlag = "Dictionary";
        }else if (modeSelectionController.flag == "n"){
            modeFlag = "Names";
        }else if(modeSelectionController.flag == "s"){
            modeFlag = "Science";
        }

        //if(levelFlag <= loginController.user.getStatus().get(modeFlag).indexOf(0)){
            openScene.openScene("gamePlay",true);

        //}

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logout.setText(loginController.usernameDisplay);


        ArrayList<Button> buttons = new ArrayList<>(Arrays.asList(levelplay1,levelplay2,levelplay3,levelplay4,levelplay5,levelplay6,levelplay7,levelplay8));

        if (modeSelectionController.flag.equals("d")){
            for (int i = 0; i < loginController.user.getStatus().get("Dictionary").size(); i++) {
                for (int j = 0; j < loginController.user.getStatus().get("Dictionary").get(0); j++) {
                    buttons.get(j).setDisable(false);
                    buttons.get(j).setStyle("-fx-background-color: #ff99fb; ");

                }
            }

        }else if (modeSelectionController.flag.equals("n")){
            for (int i = 0; i < loginController.user.getStatus().get("Names").size(); i++) {
                for (int j = 0; j < loginController.user.getStatus().get("Names").get(0); j++) {
                    buttons.get(j).setDisable(false);
                    buttons.get(j).setStyle("-fx-background-color: #ff99fb; ");
                }
            }

        }else if(modeSelectionController.flag.equals("s")){
            for (int i = 0; i < loginController.user.getStatus().get("Science").size(); i++) {
                for (int j = 0; j < loginController.user.getStatus().get("Science").get(0); j++) {
                    buttons.get(j).setDisable(false);
                    buttons.get(j).setStyle("-fx-background-color: #ff99fb; ");
                }
            }

        }

    }
}
