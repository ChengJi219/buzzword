package controller;

import buzzword.Scene;
import data.GameData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class modeSelectionController implements Initializable {
    @FXML
    private Button home2;
    @FXML
    private Button modeD, modeP, modeS;
    @FXML
    private Button close, help, settings;

    public static String flag;


    Scene openScene = new Scene();

    public void handleHelp(){
        openScene.openScene("help",true);
    }
    public void handleSettings(){
        openScene.openScene("settings", true);
    }

    public void handleClose(){
        System.exit(0);
    }
    public void handleLogoutRequest() {
        openScene.openScene("home_main", true);
    }

    public void handleLevelSelectionD() {
        flag = "d";
        openScene.openScene("levelSelection", true);
    }
    public void handleLevelSelectionP() {
        flag = "n";
        openScene.openScene("levelSelection", true);
    }
    public void handleLevelSelectionS() {
        flag = "s";
        openScene.openScene("levelSelection", true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        home2.setText(loginController.user.getUserName());

    }
}
