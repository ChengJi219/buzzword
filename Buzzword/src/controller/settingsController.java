package controller;

import com.alibaba.fastjson.JSON;
import data.GameData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by orang_000 on 12/10/2016.
 */
public class settingsController implements Initializable {

    @FXML
    private Button home2, home, usernameChange, passwordChange;
    @FXML
    private TextField usernameInput, passwordInput;
    @FXML
    private Label dic, names, science;

    private static String tempName;


    buzzword.Scene openScene = new buzzword.Scene();
    public void handleHome(){
        openScene.openScene("modeSelection", true);
    }
    public void handleLogout(){
        openScene.openScene("home_main", null);
    }
    public void handleNameChange(){
        usernameInput.setDisable(false);
        usernameInput.setVisible(true);
        usernameInput.setText(usernameChange.getText());
        usernameChange.setVisible(false);
        usernameChange.setDisable(true);
    }
    public void handlePasswordChange(){
        passwordInput.setDisable(false);
        passwordInput.setVisible(true);
        passwordInput.setText(passwordChange.getText());
        passwordChange.setVisible(false);
        passwordChange.setDisable(true);
    }

    public void onEnter(ActionEvent ae){
        String source = ((TextField)ae.getSource()).getId();
        if (source.equals("usernameInput")){
            loginController.user.setUserName(usernameInput.getText());

            //save current status
            List<GameData> peopleList = null;
            File file = new File("UsersData.txt");
            String data = null;
            try {
                data = FileUtils.readFileToString(file);
                if (data == null || data.length() == 0){
                    peopleList = new ArrayList<>();
                }else{
                    peopleList = JSON.parseArray(data, GameData.class);
                }
                if (peopleList == null || peopleList.size() == 0){
                    peopleList = new ArrayList<>();
                }
                for (int i = 0; i < peopleList.size(); i++) {
                    if(peopleList.get(i).getUserName().equals(tempName)){
                        peopleList.set(i, loginController.user);
                    }
                }
                String fromJson = JSON.toJSONString(peopleList);
                FileUtils.writeStringToFile(file,fromJson);

            } catch (IOException e) {
                e.printStackTrace();
            }
            usernameInput.clear();
            usernameInput.setVisible(false);
            usernameInput.setDisable(true);
            usernameChange.setText(loginController.user.getUserName());
            usernameChange.setDisable(false);
            usernameChange.setVisible(true);
        }else if (source.equals("passwordInput")){

            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");

            messageDigest.update(passwordInput.getText().getBytes());
            String tempPassword = passwordInput.getText();
            byte[] messageDigestMD5 = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }
            loginController.user.setPassword(stringBuffer.toString());

            //save current status
            List<GameData> peopleList = null;
            File file = new File("UsersData.txt");
            String data = null;
            try {
                data = FileUtils.readFileToString(file);
                if (data == null || data.length() == 0){
                    peopleList = new ArrayList<>();
                }else{
                    peopleList = JSON.parseArray(data, GameData.class);
                }
                if (peopleList == null || peopleList.size() == 0){
                    peopleList = new ArrayList<>();
                }
                for (int i = 0; i < peopleList.size(); i++) {
                    if(peopleList.get(i).getUserName().equals(loginController.user.getUserName())){
                        peopleList.set(i, loginController.user);

                    }
                }
                String fromJson = JSON.toJSONString(peopleList);
                FileUtils.writeStringToFile(file,fromJson);

            } catch (IOException e) {
                e.printStackTrace();
            }
            passwordInput.clear();
            passwordInput.setVisible(false);
            passwordInput.setDisable(true);
            passwordChange.setText(tempPassword);
            passwordChange.setDisable(false);
            passwordChange.setVisible(true);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }
    public void handleClose(){
        System.exit(0);
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        home2.setText(loginController.usernameDisplay);
        usernameChange.setText(loginController.usernameDisplay);
        passwordChange.setText(loginController.passwordToview);
        tempName = loginController.user.getUserName();

        //set labels
        dic.setText(loginController.user.getStatus().get("Dictionary").get(0).toString());
        names.setText(loginController.user.getStatus().get("Names").get(0).toString());
        science.setText(loginController.user.getStatus().get("Science").get(0).toString());


    }
}
