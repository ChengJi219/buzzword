package buzzword;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Created by orang_000 on 11/27/2016.
 */
public class Scene {
    public Stage currentStage = new Stage();
    //public Stage previousStage = new Stage();
    //public Stage oldStage = new Stage();
    public String loadAddress;
    public static javafx.scene.Scene temp;

    public void openScene(String sceneName, Boolean hide){
        currentStage.setTitle("Buzzword");
        currentStage.initStyle(StageStyle.UNDECORATED);
        loadAddress = "/buzzword/" + sceneName + ".fxml";

        if(hide == true){
           // previousStage.hide();
           // oldStage.hide();
        }

        try {
            Parent root = FXMLLoader.load(getClass().getResource(loadAddress));
            javafx.scene.Scene scene = new javafx.scene.Scene(root);
//            temp = new javafx.scene.Scene(root);
//            System.out.println(temp);
            currentStage.setScene(scene);
            //oldStage = previousStage;
            currentStage.show();
            //previousStage = currentStage;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
