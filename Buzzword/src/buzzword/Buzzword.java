package buzzword;

import controller.testFile;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;

/**
 * Created by orang_000 on 11/26/2016.
 */
public class Buzzword extends Application{
    Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        buzzword.Scene startScene = new buzzword.Scene();
        startScene.openScene("home_main",false);
    }

    public Stage getStage() {
        return stage;
    }
}
